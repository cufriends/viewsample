package com.example.springWithhTiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootViewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootViewsApplication.class, args);
	}
}
