package com.example.springWithhTiles.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.springWithhTiles.model.Student;
import com.example.springWithhTiles.service.StudentService;

@Controller
public class MyApplication {

	@Autowired
	private StudentService studentService;
	@RequestMapping("/")
	public String index()
	{
		return "index";
	}
	
	@GetMapping(value="zungbawm")
	public String login()
	{
		return "login";
	}
	
	@PostMapping(value="save")
	public String save(Student student,Model model)
	{
		studentService.savaingData(student);
		model.addAttribute("students", studentService.findAll());
		return "success";
	}
	
	@PostMapping(value="delete")
	public String delete(Long id, Model model) {
		System.out.println("Id : "+id);
		studentService.deleteData(id);
		model.addAttribute("students",studentService.findAll());
		return "success";
	}
}
