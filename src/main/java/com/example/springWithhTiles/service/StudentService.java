package com.example.springWithhTiles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springWithhTiles.model.Student;
import com.example.springWithhTiles.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	public StudentRepository studentReposity;
	public void savaingData(Student student) {
		studentReposity.save(student);
	}
	public List<Student> findAll() {
		// TODO Auto-generated method stub
		return studentReposity.findAll();
	}
	
	public void deleteData(Long id) {
		studentReposity.delete(id);
		
	}
}
